from flask import Flask, render_template, url_for, redirect, request
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin, login_user, LoginManager, login_required, logout_user, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import Length, ValidationError, DataRequired, EqualTo, Email
from flask_bcrypt import Bcrypt
from datetime import datetime, timezone
from sqlalchemy.sql import func



app = Flask(__name__)
bcrypt = Bcrypt(app)
app.app_context().push()
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:123456@localhost/work_management"
app.config['SECRET_KEY'] = "anh-biet-phai-lam-sao-bay-gio"
db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

#Database
class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    password = db.Column(db.String(80), nullable=False)
    confirm_password = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    projectss = db.relationship("Project", backref="user")
                                                                           

class Project(db.Model):
    __tablename__ = 'projects'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    completed = db.Column(db.Boolean, default=False)
    name =  db.Column(db.String(1000))
    tasks = db.relationship("Task", backref="project")


class Task(db.Model):
    __tablename__ = "tasks"

    id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey("projects.id"), nullable=False)
    title_task = db.Column(db.String(10000), nullable=False)
    name_task = db.Column(db.String(10000), nullable=False)
    created_task = db.Column(db.DateTime(timezone=True), default=func.now())
    action_task = db.Column(db.Boolean, default=False)
    date_completed = db.Column(db.DateTime, default=None)



@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


#Flask login (Login and Register form)
class RegisterForm(FlaskForm):
    username = StringField(validators=[
                           DataRequired(), Length(min=4, max=20)], render_kw={"placeholder": "Username"})

    password = PasswordField(validators=[
                             DataRequired(), Length(min=8, max=20)], render_kw={"placeholder": "Password"})
    confirm_password = PasswordField(validators=[DataRequired(), EqualTo("password")], render_kw={"placeholder": "Confirm Password"})

    email = StringField(validators=[DataRequired(), Email()], render_kw={"placeholder": "Email"})

    submit = SubmitField('Register')

    def validate_username(self, username):
        existing_user_username = User.query.filter_by(
            username=username.data).first()
        if existing_user_username:
            raise ValidationError(
                'That username already exists. Please choose a different one.')


class LoginForm(FlaskForm):
    email = StringField(validators=[
                           DataRequired()], render_kw={"placeholder": "Email"})

    password = PasswordField(validators=[
                             DataRequired(), Length(min=8, max=20)], render_kw={"placeholder": "Password"})

    submit = SubmitField('Login')

#Login and register user
@app.route('/')
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            if bcrypt.check_password_hash(user.password, form.password.data):
                login_user(user)
                return redirect(url_for('dashboard'))
            
    return render_template('login.html', form=form)



@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()

    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode("utf-8")
        hashed_password2 = bcrypt.generate_password_hash(form.confirm_password.data).decode("utf-8")
        new_user = User(username=form.username.data, password=hashed_password, confirm_password=hashed_password2, email=form.email.data)
        db.session.add(new_user)
        db.session.commit()
        return redirect(url_for('login'))

    return render_template('register.html', form=form)



#PROJECT
@app.route("/dashboard", methods=["GET", "POST"])
@login_required
def dashboard():
    if request.method == 'POST':
        project = request.form['project']
        if project.strip() != '':
            new_item = Project(user_id = current_user.id, name=project)
            db.session.add(new_item)
            db.session.commit()
        return redirect('/dashboard')
    else:
        items = Project.query.filter(current_user.id == Project.user_id).all()  
        return render_template("dashboard.html", items=items)


@app.route("/complete_project/<int:item_id>")
def complete(item_id):
    item = Project.query.get_or_404(item_id)
    item.completed = True
    db.session.commit()
    return redirect('/dashboard')


@app.route("/delete_project/<int:item_id>")
def delete(item_id):
    item = Project.query.get_or_404(item_id)
    Task.query.filter(Task.project_id == item_id).delete()
    db.session.delete(item)
    db.session.commit()
    return redirect('/dashboard')



#TASK
@app.route("/task/<int:item_id>", methods=["GET", "POST"])
def task(item_id):
    if request.method == "POST":
        title_task = request.form["title_task"]
        name_task = request.form["name_task"]
        if title_task.strip() != '' and len(title_task.split()) < 50 and len(name_task.split()) < 1000:
            new_task = Task(project_id = item_id, title_task = title_task, name_task = name_task)
            db.session.add(new_task)
            db.session.commit()
            return redirect(f"/task/{item_id}")
    else:
        items = Task.query.filter(Task.project_id == item_id).all()  
        return render_template("tasks.html", items=items)


@app.route("/complete_task/<int:id>/<int:item_id>")
def complete_task(id, item_id):
    item = Task.query.get_or_404(item_id)
    item.action_task = True
    db.session.commit()
    return redirect(f"/task/{id}")

@app.route("/delete_task/<int:id>/<int:item_id>")
def delete_task(id, item_id):
    item = Task.query.get_or_404(item_id)
    db.session.delete(item)
    db.session.commit()
    return redirect(f"/task/{id}")


#Logout
@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


if __name__ == "__main__":
    app.run(debug=True)